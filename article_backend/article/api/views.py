from rest_framework import viewsets

from ..models import Article
from . import serializers


class ArticleViewset(viewsets.ModelViewSet):
    serializer_class = serializers.ArticleSerializers
    queryset = Article.objects.all()
    lookup_field = 'pk'
