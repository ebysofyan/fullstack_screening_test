package ebysofyan.test.article_android.views.article.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import ebysofyan.test.article_android.R
import ebysofyan.test.article_android.common.extensions.httpErrorCodeParser
import ebysofyan.test.article_android.common.utils.Constants
import ebysofyan.test.article_android.data.Article
import kotlinx.android.synthetic.main.fragment_article_list.*
import retrofit2.Response

class ArticleListFragment : Fragment(), ArticleListContract.View {

    private lateinit var presenter: ArticleListContract.Presenter
    private var adapter: ArticleRecyclerAdapter? = null
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_article_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initPresenter()
        registerListener()
    }

    private fun init() {
        navController = Navigation.findNavController(activity!!, R.id.main_nav_host_fragment)
    }

    private fun initPresenter() {
        presenter = ArticleListPresenter(context!!)
        presenter.setView(this)
        presenter.fetchArticles()
    }

    private fun registerListener() {
        article_fab.setOnClickListener {
            navController.navigate(R.id.action_articleListFragment_to_articleAddFragment)
        }

        article_list_swipe.setOnRefreshListener {
            adapter?.clearItems()
            presenter.fetchArticles()
        }
    }


    override fun onLoading(show: Boolean, msg: String) {
        article_list_swipe?.isRefreshing = show
    }

    override fun onArticlesLoaded(articles: MutableList<Article>?) {
        articles?.let {
            adapter = ArticleRecyclerAdapter(it) {
                val bundle = Bundle().apply {
                    putParcelable(Constants.ARTICLE_IBJ, it)
                }
                navController.navigate(R.id.action_articleListFragment_to_articleUpdateFragment, bundle)
            }
            article_list_rcview?.layoutManager = LinearLayoutManager(context)
            article_list_rcview?.adapter = adapter
        }
    }

    override fun onArticleLoadFailed(response: Response<MutableList<Article>>) {
        view?.let { context?.httpErrorCodeParser(it, response) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.destroyView()
    }
}