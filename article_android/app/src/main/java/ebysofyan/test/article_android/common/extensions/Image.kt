package ebysofyan.test.article_android.common.extensions

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ebysofyan.test.article_android.R
import id.zelory.compressor.Compressor
import java.io.File

fun ImageView.loadWithGlidePlaceholder(
    imageUrl: String,
    placeHolderImage: Int = R.drawable.no_image
) {
    val glideOption = RequestOptions().apply {
        placeholder(placeHolderImage)
    }
    Glide.with(this.context)
        .applyDefaultRequestOptions(glideOption)
        .load(imageUrl)
        .into(this)
}

fun ImageView.loadWithGlidePlaceholder(
    imageFile: File?,
    placeHolderImage: Int = R.drawable.no_image
) {
    val glideOption = RequestOptions().apply {
        placeholder(placeHolderImage)
    }
    Glide.with(this.context)
        .applyDefaultRequestOptions(glideOption)
        .load(imageFile)
        .into(this)
}

fun ImageView.loadWithGlidePlaceholder(
    resourceId: Int?,
    placeHolderImage: Int = R.drawable.no_image
) {
    val glideOption = RequestOptions().apply {
        placeholder(placeHolderImage)
    }
    Glide.with(this.context)
        .applyDefaultRequestOptions(glideOption)
        .load(resourceId)
        .into(this)
}

fun File.compress(context: Context, quality: Int = 30): File = Compressor(context).setQuality(quality)
    .setCompressFormat(Bitmap.CompressFormat.JPEG)
    .compressToFile(this)