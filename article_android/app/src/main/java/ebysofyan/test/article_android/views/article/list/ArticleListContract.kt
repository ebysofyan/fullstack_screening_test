package ebysofyan.test.article_android.views.article.list

import ebysofyan.test.article_android.base.BasePresenter
import ebysofyan.test.article_android.base.BaseView
import ebysofyan.test.article_android.data.Article
import retrofit2.Response

interface ArticleListContract {
    interface View : BaseView {
        fun onArticlesLoaded(articles: MutableList<Article>?)
        fun onArticleLoadFailed(response: Response<MutableList<Article>>)
    }

    interface Presenter : BasePresenter<View> {
        fun fetchArticles()
    }
}