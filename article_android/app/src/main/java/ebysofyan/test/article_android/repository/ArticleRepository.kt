package ebysofyan.test.article_android.repository

import ebysofyan.test.article_android.data.Article
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ArticleRepository {
    @GET("/api/articles/")
    fun fetchArticles(): Call<MutableList<Article>>

    @Multipart
    @POST("/api/articles/")
    fun postArticles(
        @PartMap mapBody: HashMap<String, RequestBody>,
        @Part image: MultipartBody.Part
    ): Call<Article>

    @Multipart
    @PUT("/api/articles/{id}/")
    fun updateArticles(
        @Path("id") id: String,
        @PartMap mapBody: HashMap<String, RequestBody>,
        @Part image: MultipartBody.Part
    ): Call<Article>
}