package ebysofyan.test.article_android.views.article.update

import ebysofyan.test.article_android.base.BasePresenter
import ebysofyan.test.article_android.base.BaseView
import ebysofyan.test.article_android.data.Article
import retrofit2.Response
import java.io.File

interface ArticleUpdateContract {
    interface View : BaseView {
        fun onArticleUpdated(article: Article?)

        fun onArticleUpdateFailed(response: Response<Article>)
    }

    interface Presenter : BasePresenter<View> {
        fun updateArticle(id: String, article: Article, image: File?)
    }
}