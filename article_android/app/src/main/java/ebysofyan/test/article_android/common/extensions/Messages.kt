package ebysofyan.test.article_android.common.extensions

import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import ebysofyan.test.article_android.R

fun View.showSnackbar(
    msg: String,
    withAction: Boolean = false,
    textColor: Int = R.color.textPrimaryWhite,
    length: Int = Snackbar.LENGTH_LONG,
    actionListener: (Snackbar) -> Unit = {}
) {
    val snackBar = Snackbar.make(this, msg, length)
    if (withAction) {
        snackBar.setAction("Ok") {
            actionListener.invoke(snackBar)
        }.setActionTextColor(ContextCompat.getColor(context, textColor))
    }
    snackBar.show()
}
