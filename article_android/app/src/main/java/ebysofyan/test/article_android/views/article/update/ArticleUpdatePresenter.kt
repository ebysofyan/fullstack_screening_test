package ebysofyan.test.article_android.views.article.update

import android.content.Context
import ebysofyan.test.article_android.common.extensions.addMapRequestBody
import ebysofyan.test.article_android.common.extensions.addToRequestBody
import ebysofyan.test.article_android.common.extensions.onNetworkError
import ebysofyan.test.article_android.common.utils.NetworkConfig
import ebysofyan.test.article_android.data.Article
import ebysofyan.test.article_android.repository.ArticleRepository
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ArticleUpdatePresenter(private val context: Context) : ArticleUpdateContract.Presenter {
    private var view: ArticleUpdateContract.View? = null
    private var request: Call<Article>? = null

    override fun updateArticle(id: String, article: Article, image: File?) {
        val textRequestBody = HashMap<String, RequestBody>()
            .addMapRequestBody("title", article.title.toString())
            .addMapRequestBody("content", article.content.toString())

        val imageRequestBody = if (image != null) {
            MultipartBody.Part.createFormData(
                "image", image?.name,
                image?.addToRequestBody()
            )
        } else {
            MultipartBody.Part.createFormData("", "")
        }

        view?.onLoading(true, "Updating article . . .")
        request = NetworkConfig.retrofit.create(ArticleRepository::class.java)
            .updateArticles(id, textRequestBody, imageRequestBody)
        request?.enqueue(object : Callback<Article> {
            override fun onFailure(call: Call<Article>, t: Throwable) {
                view?.onLoading(false)
                context.onNetworkError()
            }

            override fun onResponse(call: Call<Article>, response: Response<Article>) {
                view?.onLoading(false)
                when {
                    response.isSuccessful -> view?.onArticleUpdated(response.body())
                    else -> view?.onArticleUpdateFailed(response)
                }
            }

        })
    }

    override fun setView(view: ArticleUpdateContract.View) {
        this.view = view
    }

    override fun destroyView() {
        request?.cancel()
    }
}