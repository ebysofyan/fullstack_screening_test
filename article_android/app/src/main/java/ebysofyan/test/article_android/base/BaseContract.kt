package ebysofyan.test.article_android.base

interface BaseView {
    fun onLoading(show: Boolean, msg: String = "")
}

interface BasePresenter<V> {
    fun setView(view: V)

    fun destroyView()
}