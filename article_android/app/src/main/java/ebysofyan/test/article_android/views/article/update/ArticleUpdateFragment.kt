package ebysofyan.test.article_android.views.article.update

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ebysofyan.test.article_android.R
import ebysofyan.test.article_android.common.extensions.compress
import ebysofyan.test.article_android.common.extensions.httpErrorCodeParser
import ebysofyan.test.article_android.common.extensions.isFormInputRequiredValid
import ebysofyan.test.article_android.common.extensions.loadWithGlidePlaceholder
import ebysofyan.test.article_android.common.utils.Constants
import ebysofyan.test.article_android.data.Article
import kotlinx.android.synthetic.main.fragment_article_update.*
import kotlinx.android.synthetic.main.fragment_article_update.*
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.progressDialog
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import retrofit2.Response
import java.io.File

class ArticleUpdateFragment : Fragment(), ArticleUpdateContract.View {
    private var articleObj: Article? = null
    private val IMAGE_REQUEST_CODE = 100
    private var image: File? = null

    private lateinit var presenter: ArticleUpdateContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_article_update, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPresenter()
        setData()
        registerListener()
    }

    private fun initPresenter() {
        presenter = ArticleUpdatePresenter(context!!)
        presenter.setView(this)
    }

    private fun setData() {
        articleObj = arguments?.getParcelable(Constants.ARTICLE_IBJ)
        article_update_image.loadWithGlidePlaceholder(articleObj?.image.toString())
        article_update_text_body.setText(articleObj?.title.toString())
        article_update_text_content.setText(articleObj?.content.toString())
    }

    private fun registerListener() {
        article_update_image_container.setOnClickListener {
            EasyImage.openChooserWithGallery(this, "Choose an image", IMAGE_REQUEST_CODE)
        }

        article_update_button_post.setOnClickListener {
            onSubmit()
        }
    }

    private fun onSubmit() {
        val forms = mutableListOf(
            article_update_text_body_layout to "Title cannot be empty",
            article_update_text_content_layout to "Content cannot be empty"
        )

        context?.isFormInputRequiredValid(forms) {
            val article = Article(
                title = article_update_text_body.text.toString(),
                content = article_update_text_content.text.toString()
            )

            presenter.updateArticle(articleObj?.id.toString(), article, image)
        }
    }

    override fun onArticleUpdated(article: Article?) {
        alert("Article updated!") {
            isCancelable = false
            okButton {
                it.dismiss()
                findNavController().navigateUp()
            }
        }.show()
    }

    override fun onArticleUpdateFailed(response: Response<Article>) {
        context?.httpErrorCodeParser(view!!, response)
    }

    private var progressDialog: ProgressDialog? = null
    override fun onLoading(show: Boolean, msg: String) {
        if (progressDialog == null) {
            progressDialog = progressDialog(msg) {
                isIndeterminate = true
                setProgressStyle(ProgressDialog.STYLE_SPINNER)
                setCanceledOnTouchOutside(false)
            }
        }

        if (show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.destroyView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagesPicked(p0: MutableList<File>, p1: EasyImage.ImageSource?, p2: Int) {
                image = p0[0].compress(context!!)
                article_update_image.loadWithGlidePlaceholder(image)
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {}
        })
    }
}