package ebysofyan.test.article_android.views.article.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ebysofyan.test.article_android.R
import ebysofyan.test.article_android.common.extensions.dateFormat
import ebysofyan.test.article_android.common.extensions.loadWithGlidePlaceholder
import ebysofyan.test.article_android.data.Article
import kotlinx.android.synthetic.main.article_item_view.view.*

class ArticleRecyclerAdapter(
    private val list: MutableList<Article>,
    private val updateListener: (Article) -> Unit
) : RecyclerView.Adapter<ArticleRecyclerAdapter.ArticleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.article_item_view, parent, false)
        return ArticleViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.onBindItem(list[position], updateListener)
    }

    fun clearItems() {
        list.clear()
        notifyDataSetChanged()
    }

    class ArticleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun onBindItem(article: Article, updateListener: (Article) -> Unit) {
            itemView.img_article.loadWithGlidePlaceholder(article.image.toString())
            itemView.txt_title.text = article.title.toString()
            itemView.txt_detail.text = article.created?.dateFormat()
            itemView.txt_content.text = article.content.toString()

            itemView.txt_update.setOnClickListener {
                updateListener.invoke(article)
            }
        }
    }
}