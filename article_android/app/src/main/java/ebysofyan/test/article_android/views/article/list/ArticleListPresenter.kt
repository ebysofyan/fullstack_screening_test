package ebysofyan.test.article_android.views.article.list

import android.content.Context
import ebysofyan.test.article_android.common.extensions.onNetworkError
import ebysofyan.test.article_android.common.utils.NetworkConfig
import ebysofyan.test.article_android.data.Article
import ebysofyan.test.article_android.repository.ArticleRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArticleListPresenter(private val context: Context) : ArticleListContract.Presenter {

    private var view: ArticleListContract.View? = null
    private var request: Call<MutableList<Article>>? = null

    override fun fetchArticles() {
        this.view?.onLoading(true)
        request = NetworkConfig.retrofit.create(ArticleRepository::class.java).fetchArticles()
        request?.enqueue(object : Callback<MutableList<Article>> {
            override fun onFailure(call: Call<MutableList<Article>>, t: Throwable) {
                view?.onLoading(false)
                context.onNetworkError()
            }

            override fun onResponse(call: Call<MutableList<Article>>, response: Response<MutableList<Article>>) {
                view?.onLoading(false)
                when {
                    response.isSuccessful -> view?.onArticlesLoaded(response.body())
                    else -> view?.onArticleLoadFailed(response)
                }
            }
        })
    }

    override fun setView(view: ArticleListContract.View) {
        this.view = view
    }

    override fun destroyView() {
        request?.cancel()
    }
}