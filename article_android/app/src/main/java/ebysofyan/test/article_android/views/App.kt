package ebysofyan.test.article_android.views

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import pl.aprilapps.easyphotopicker.EasyImage

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        EasyImage.configuration(this).apply {
            setImagesFolderName("EasyImage Picker")
            setAllowMultiplePickInGallery(false)
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }
}