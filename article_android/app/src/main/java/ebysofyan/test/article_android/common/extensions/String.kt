package ebysofyan.test.article_android.common.extensions

import java.text.SimpleDateFormat
import java.util.*

fun String.dateFormat(format: String? = "EEEE, dd MMMM yyyy"): String {
    val date = SimpleDateFormat("yyyy-MM-dd").parse(this)
    return SimpleDateFormat(format, Locale("in", "ID")).format(date)
}

fun String.dateValidFormat(format: String? = "yyyy-MM-dd"): String {
    val date = SimpleDateFormat("dd-MM-yyyy").parse(this)
    return SimpleDateFormat(format, Locale("in", "ID")).format(date)
}
