package ebysofyan.test.article_android.views.article.add

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ebysofyan.test.article_android.R
import ebysofyan.test.article_android.common.extensions.compress
import ebysofyan.test.article_android.common.extensions.httpErrorCodeParser
import ebysofyan.test.article_android.common.extensions.isFormInputRequiredValid
import ebysofyan.test.article_android.common.extensions.loadWithGlidePlaceholder
import ebysofyan.test.article_android.data.Article
import kotlinx.android.synthetic.main.fragment_article_add.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.progressDialog
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import retrofit2.Response
import java.io.File

class ArticleAddFragment : Fragment(), ArticleAddContract.View {

    private val IMAGE_REQUEST_CODE = 101
    private var image: File? = null

    private lateinit var presenter: ArticleAddContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_article_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPresenter()
        registerListener()
    }

    private fun initPresenter() {
        presenter = ArticleAddPresenter(context!!)
        presenter.setView(this)
    }

    private fun registerListener() {
        article_add_image_container.setOnClickListener {
            EasyImage.openChooserWithGallery(this, "Choose an image", IMAGE_REQUEST_CODE)
        }

        article_add_button_post.setOnClickListener {
            onSubmit()
        }
    }

    private fun onSubmit() {
        val forms = mutableListOf(
            article_add_text_body_layout to "Title cannot be empty",
            article_add_text_content_layout to "Content cannot be empty"
        )

        context?.isFormInputRequiredValid(forms) {
            if (image == null) {
                context?.longToast("Please select image")
                return@isFormInputRequiredValid
            }

            val article = Article(
                title = article_add_text_body.text.toString(),
                content = article_add_text_content.text.toString()
            )
            presenter.postArticle(article, image!!)
        }
    }

    override fun onArticleCreated(article: Article?) {
        alert("New article created!") {
            isCancelable = false
            okButton {
                it.dismiss()
                findNavController().navigateUp()
            }
        }.show()
    }

    override fun onArticleCreateFailed(response: Response<Article>) {
        context?.httpErrorCodeParser(view!!, response)
    }

    private var progressDialog: ProgressDialog? = null
    override fun onLoading(show: Boolean, msg: String) {
        if (progressDialog == null) {
            progressDialog = progressDialog(msg) {
                isIndeterminate = true
                setProgressStyle(ProgressDialog.STYLE_SPINNER)
                setCanceledOnTouchOutside(false)
            }
        }

        if (show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.destroyView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagesPicked(p0: MutableList<File>, p1: EasyImage.ImageSource?, p2: Int) {
                image = p0[0].compress(context!!)
                article_add_image_add.visibility = View.GONE
                article_add_image.loadWithGlidePlaceholder(image)
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {}
        })
    }
}