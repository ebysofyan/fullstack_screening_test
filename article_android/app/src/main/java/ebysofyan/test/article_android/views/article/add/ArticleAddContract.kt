package ebysofyan.test.article_android.views.article.add

import ebysofyan.test.article_android.base.BasePresenter
import ebysofyan.test.article_android.base.BaseView
import ebysofyan.test.article_android.data.Article
import retrofit2.Response
import java.io.File

interface ArticleAddContract {
    interface View : BaseView {
        fun onArticleCreated(article: Article?)

        fun onArticleCreateFailed(response: Response<Article>)
    }

    interface Presenter : BasePresenter<View> {
        fun postArticle(article: Article, image: File?)
    }
}