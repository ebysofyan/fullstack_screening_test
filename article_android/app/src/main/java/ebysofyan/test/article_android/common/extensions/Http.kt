package ebysofyan.test.article_android.common.extensions

import android.content.Context
import android.view.View
import android.widget.Toast
import ebysofyan.test.article_android.R
import retrofit2.Response

fun Context.httpErrorCodeParser(view: View, response: Response<*>, custom400: String = "") {
    when (response.code()) {
        400 -> when {
            custom400.isNotEmpty() -> view.showSnackbar(custom400)
            else -> view.showSnackbar(getString(R.string.http_400_msg))
        }
        404 -> view.showSnackbar(getString(R.string.http_404_msg))
        408 -> view.showSnackbar(getString(R.string.http_408_msg))
        500 -> view.showSnackbar(getString(R.string.http_500_msg))
    }
}

fun Context.onNetworkError() {
    Toast.makeText(this, getString(R.string.network_error_msg), Toast.LENGTH_LONG).show()
}