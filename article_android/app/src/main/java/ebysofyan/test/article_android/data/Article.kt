package ebysofyan.test.article_android.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Article(
    val id: String? = null,
    val title: String? = null,
    val content: String? = null,
    val image: String? = null,
    val created: String? = null,
    val modified: String? = null
) : Parcelable